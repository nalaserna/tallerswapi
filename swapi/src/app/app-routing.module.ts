import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaFilmComponent } from './lista-film/lista-film.component';
import { DetalleFilmComponent } from './detalle-film/detalle-film.component';

const routes: Routes = [
  {path: 'listaFilms', component: ListaFilmComponent},
  {path: 'verFilm/:filmId', component: DetalleFilmComponent},
  {path: '', pathMatch: 'full', redirectTo: 'listaFilms'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
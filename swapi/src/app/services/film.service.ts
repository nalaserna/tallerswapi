import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) {

   }

   public getAllFilms(): Observable<any[]> {
    return this.http.get<any[]>(environment.listaFilms);
  }

  public viewFilm(filmId: string){
    return this.http.get<any[]>('https://swapi.co/api/films/' +filmId);
  }

  public viewCharacter(url:string): Observable<any[]>{
    return this.http.get<any[]>(url); 
  }

  
} 

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleFilmComponent } from './detalle-film.component';

describe('DetalleFilmComponent', () => {
  let component: DetalleFilmComponent;
  let fixture: ComponentFixture<DetalleFilmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleFilmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

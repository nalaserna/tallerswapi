import { Component, OnInit, Input } from '@angular/core';
import { NgModule } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FilmService } from '../services/film.service';

@Component({
  selector: 'app-detalle-film',
  templateUrl: './detalle-film.component.html',
  styleUrls: ['./detalle-film.component.css']
})
export class DetalleFilmComponent implements OnInit {

  @Input() characters: any[] = [];
  @Input() mycharacters : any [] = [];
  @Input() myfilm : any[] = [];
  id : string;
  
  

  constructor(private http: HttpClient,private servicio: FilmService, private router: ActivatedRoute) {
    
   
   
   }

  ngOnInit() {

    this.id = this.router.snapshot.params['filmId'];
    this.servicio.viewFilm(this.id).subscribe((resp) => 
      {this.myfilm = resp;
        this.characters = resp.characters;
      console.log(this.characters.length);
    for(let i=0; i<this.characters.length; i++){
     this.servicio.viewCharacter(this.characters[i]).subscribe(
       resp => this.mycharacters[i]= resp);
    }});

     
     
  }


}

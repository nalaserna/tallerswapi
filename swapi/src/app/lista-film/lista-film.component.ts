import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FilmService } from '../services/film.service';

@Component({
  selector: 'app-lista-film',
  templateUrl: './lista-film.component.html',
  styleUrls: ['./lista-film.component.css']
})
export class ListaFilmComponent implements OnInit {

  lista: any[] = [];
  

  constructor(private http: HttpClient,private servicio: FilmService, private router: Router) {
    servicio.getAllFilms().subscribe(resp => 
      this.lista = resp.results);
    console.log(this.lista);
   
   }

  ngOnInit() {
    
      
  }



}
